---
- name: Provision cockroachdb single node
  hosts: db
  remote_user: root
  # become: yes
  # become_user: root
  vars:
    download_path: https://binaries.cockroachdb.com/cockroach-v20.2.8.linux-amd64.tgz
    destination_path: /root/cockroach-v20.2.8.linux-amd64
    access_user: kendellfab
    access_cert_path: /home/kendellfab/

  tasks:
  - name: Disable SELinux
    ansible.posix.selinux: state=disabled

  - name: Install epel
    dnf: name=epel-release state=latest
  
  - name: Install fish
    dnf: name=fish state=latest
  
  - name: Install nano
    dnf: name=nano state=latest
  
  - name: Install python 3
    dnf: name=python3 state=latest
  
  - name: Install for chsh
    dnf: name=util-linux-user state=latest
  
  - name: Create cockroach group
    group: name=cockroach state=present
  
  - name: Create cockroach user
    user: name=cockroach group=cockroach system=yes
  
  - name: Add cockroach directory
    file: path=/var/lib/cockroach owner=cockroach group=cockroach mode=0755 state=directory
  
  - name: Download and unarchive cockroachdb
    unarchive: src={{download_path}} dest=~ remote_src=yes creates={{destination_path}}
  
  - name: Copy binary
    copy: src=~/cockroach-v20.2.8.linux-amd64/cockroach dest=/usr/local/bin/cockroach remote_src=yes mode=0755
  
  - name: Generate certs for single instance
    delegate_to: localhost
    command: cmd=./generate_certs.sh creates=key/ca.key
  
  - name: Sync cert files for running
    synchronize: src=certs dest=/var/lib/cockroach/ group=no owner=no
  
  - name: Change ownership of certs
    file: path=/var/lib/cockroach/certs owner=cockroach group=cockroach mode=0700 state=directory recurse=yes
  
  - name: Sync cert files for localhost access
    synchronize: src=certs dest={{access_cert_path}} group=no owner=no
  
  - name: Change ownership of certs
    file: path={{access_cert_path}}/certs owner={{access_user}} group={{access_user}} mode=0700 state=directory recurse=yes
  
  - name: Copy service file over
    template: src=cockroach.service.tpl dest=/etc/systemd/system/cockroach.service owner=root group=root
  
  - name: Reload systemd
    systemd: daemon_reload=yes
  
  - name: Install dnf automatic
    dnf: name=dnf-automatic state=latest
  
  - name: Change upgrade type to security
    replace: path=/etc/dnf/automatic.conf regexp="upgrade_type = default" replace="upgrade_type = security"
  
  - name: Change apply updates
    replace: path=/etc/dnf/automatic.conf regexp="apply_updates = no" replace="apply_updates = yes"
  
  - name: Change emit_via
    replace: path=/etc/dnf/automatic.conf regexp="emit_via = stdio" replace="emit_via = motd"
  
  - name: Enable update timer
    systemd: name=dnf-automatic.timer state=started enabled=yes
  
  - name: Enable cockroach
    systemd: name=cockroach state=started enabled=yes

  - name: Poke hole in firewall for database
    firewalld: port=26257/tcp permanent=yes state=enabled

  - name: Poke hole in firewall for web UI
    firewalld: port=8080/tcp permanent=yes state=enabled